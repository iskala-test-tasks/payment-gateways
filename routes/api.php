<?php

use App\Http\Api\v1\Payments\PaymentOne;
use App\Http\Api\v1\Payments\PaymentTwo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Добавил в RouteServiceProvider дополнительный префикс v1
 *
 * Пример: ${domain}/api/v1/*
 */

Route::get('/payment_one', [PaymentOne::class, 'index']);
Route::get('/payment_two', [PaymentTwo::class, 'index'])->middleware('auth:sanctum');

Route::post('/tokens/create', function (Request $request) {
    $user = User::where('id', 1)->first();

    return response()->json($user->createToken('test')->plainTextToken);
});
