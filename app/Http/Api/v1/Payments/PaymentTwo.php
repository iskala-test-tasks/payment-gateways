<?php
declare(strict_types=1);

namespace App\Http\Api\v1\Payments;

use App\Http\Requests\PaymentOneRequest;
use App\Interfaces\Api\PaymentInterface;
use App\Models\Payment;
use App\Services\PaymentService;
use Illuminate\Http\JsonResponse;

class PaymentTwo implements PaymentInterface
{
    /**
     * @param  PaymentOneRequest  $request
     * @return JsonResponse
     */
    public function index(PaymentOneRequest $request): JsonResponse
    {
        $validated = $request->validated();

        $this->successResponse(PaymentService::getHashPaymentTwo($validated));

        return response()->json('success');

    }

    /**
     * Callback method
     *
     * @param  array  $data
     * @return void
     */
    public function successResponse(Array $data): void
    {
        $userID = 1;

        $Payment = new Payment();
        $Payment->gateway_id = 2;
        $Payment->amount = $data['amount'];
        $Payment->amount_paid = $data['amount_paid'];
        $Payment->status = $data['status'];
        $Payment->user_id = $userID;
        $Payment->save();
    }

    public function failedResponse(array $data): void
    {

    }
}
