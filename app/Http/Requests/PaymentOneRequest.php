<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentOneRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'payment_id' => 'required|integer',
            'status' => 'required|string',
            'amount' => 'required|integer',
            'amount_paid' => 'required|integer',
        ];
    }
}
