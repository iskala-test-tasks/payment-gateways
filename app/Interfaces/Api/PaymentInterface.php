<?php

namespace App\Interfaces\Api;

use App\Http\Requests\PaymentOneRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

interface PaymentInterface
{
    public function index(PaymentOneRequest $request): JsonResponse;

    public function successResponse(Array $data): void;
    public function failedResponse(Array $data): void;
}
