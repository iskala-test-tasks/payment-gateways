<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * @property string $gateway_id
 * @property string $amount
 * @property string $amount_paid
 * @property string $status
 * @property string $user_id
 * @property string $created_at
 * @property string $updated_at
 */

class Payment extends Model
{
    use HasFactory;

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
